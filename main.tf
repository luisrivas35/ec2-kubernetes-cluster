# Configure the AWS Provider 

provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.region
}

variable "access_key" {
  description = "AWS access key"
  type        = string
}

variable "secret_key" {
  description = "AWS secret key"
  type        = string
}

variable "region" {
  description = "AWS region"
  type        = string
}

/*
resource "aws_security_group" "devops_sg" {
    name        = "devops-sg"
    description = "Security group for DevOpsS instance"

    # Acceso SSH
    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port   = 8080
        to_port     = 8080
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    # etcd
    ingress {
        from_port   = 2379
        to_port     = 2380
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    # API Server 
    ingress {
        from_port   = 6443
        to_port     = 6443
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    # kubelet
    ingress {
        from_port   = 10250
        to_port     = 10250
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    # kube-proxy
    ingress {
        from_port   = 10257
        to_port     = 10257
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    # kube-scheduler
    ingress {
        from_port   = 10259
        to_port     = 10259
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    # NodePorts
    ingress {
        from_port   = 30000
        to_port     = 32767
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    # Mongo DB
    ingress {
        from_port   = 27017
        to_port     = 27017
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "devops-sg"
    }
}

# vpc_security_group_ids = [aws_security_group.devops_sg.id]

*/

resource "aws_instance" "master1" {
  ami           = "ami-053b0d53c279acc90" // AMI de Ubuntu Server 22.04 LTS
  instance_type = "t3.large"
  key_name      = "ansible-key"
  vpc_security_group_ids = ["sg-0601ad4222e79dee5"]
  tags = {
    Name = "master1"
  }

  user_data = <<-EOF
              #!/bin/bash
              hostnamectl set-hostname master1
              sed -i 's/127.0.0.1 localhost/127.0.0.1 localhost master1/' /etc/hosts
              EOF
}

resource "aws_instance" "worker1" {
  ami           = "ami-053b0d53c279acc90" // AMI de Ubuntu Server 22.04 LTS
  instance_type = "t3.medium"
  key_name      = "ansible-key"
  vpc_security_group_ids = ["sg-0601ad4222e79dee5"]
  tags = {
    Name = "worker1"
  }

  user_data = <<-EOF
              #!/bin/bash
              hostnamectl set-hostname worker1
              sed -i 's/127.0.0.1 localhost/127.0.0.1 localhost worker1/' /etc/hosts
              EOF
}

resource "aws_instance" "worker2" {
  ami           = "ami-053b0d53c279acc90" // AMI de Ubuntu Server 22.04 LTS
  instance_type = "t3.medium"
  key_name      = "ansible-key"
  vpc_security_group_ids = ["sg-0601ad4222e79dee5"]
  tags = {
    Name = "worker2"
  }

  user_data = <<-EOF
              #!/bin/bash
              hostnamectl set-hostname worker2
              sed -i 's/127.0.0.1 localhost/127.0.0.1 localhost worker2/' /etc/hosts
              EOF
}

output "public_ip_master1" {
  value = aws_instance.master1.public_ip
}

output "public_ip_worker1" {
  value = aws_instance.worker1.public_ip
}

output "public_ip_worker2" {
  value = aws_instance.worker2.public_ip
}




